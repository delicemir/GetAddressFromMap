package com.example.emir.getaddressfrommap;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

public class StartupActivity extends AppCompatActivity {

    private ListView listOfPlaces;

    static ArrayList<String> places = new ArrayList<>();
    static ArrayList<LatLng> placeCoordinates = new ArrayList<>();
    static ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_startup);

        initialize();

        listOfPlaces.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent maps = new Intent(getApplicationContext(),MapsActivity.class);
                maps.putExtra("placeData",i);
                startActivity(maps);
            }
        });

    }

    private void initialize() {
        listOfPlaces = findViewById(R.id.list_of_places);

        places.add("Your location");

        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, places);
        listOfPlaces.setAdapter(adapter);

        placeCoordinates.add(new LatLng(0,0));

    }
}
